import           Development.Shake
import           Development.Shake.FilePath
import           Development.Shake.Util
import           System.Console.GetOpt

compiler = "clang++"
cflags   = "-g -std=c++11 -Wall"


hscompiler = "stack ghc"

sources = "src" -- Where is located source code and libraries
app     = "app" -- is located main applications

{-
	Configurala construccion del proyectom en este caso genera la aplicacion para testear
	y el programa para obtener las estadisticas
-}
main :: IO ()
main = shakeArgs shakeOptions{shakeFiles="_build/", shakeVerbosity = Chatty} $ do
    want [ "_build/app" <.> exe
         , "_build/stats" <.> exe
         ]

    phony "clean" $ do
      putNormal "Cleaning files in _build"
      removeFilesAfter "_build" ["//*"]

    makeBuildExec "app"
    makeBuildExec "stats"
    --makeBuildMacro "stats" "STATS"
    --makeBuildMacro "run" "RUN"
    "_build" </> sources </> "//*.o" %> \out -> do
      putNormal ("Generating .O's " ++ out)
      let c = sources </> dropDirectory1 out -<.> "cpp"
      let m = out -<.> "m"
      () <- cmd compiler cflags "-c" [c] "-o" [out] "-MMD -MF" [m]
      needMakefileDependencies m

-- makeBuildMacro :: String -> String -> UPS
makeBuildMacro exec macro = do
    phony exec $ do
      need ["_build/" ++ exec]
      putNormal ("Executing..." ++ exec)
      cmd ("./_build/" ++ exec)

    ("_build/" ++ exec) <.> exe %> \out -> do
      cs <- getDirectoryFiles "src" ["//*.cpp"]
      let os = ["_build" </> c -<.> ("o"++macro) | c <- cs]
      need os
      cmd compiler cflags ("-D" ++ macro) "-o" [out] os

    ("_build//*.o" ++ macro) %> \out -> do
      let c = "src" </> dropDirectory1 out -<.> "cpp"
      let m = out -<.> "m"
      () <- cmd compiler cflags "-c" [c] ("-D" ++ macro) "-o" [out] "-MMD -MF" [m]
      needMakefileDependencies m

makeBuildExec exec = do
    phony exec $ do
      need ["_build/" ++ exec]
      putNormal ("Executing..." ++ exec)
      cmd ("./_build/" ++ exec)

    ("_build/" ++ exec) <.> exe %> \out -> do
      putNormal ("Build" ++ exec ++ " rule for file " ++ out)
      cs <- getDirectoryFiles sources ["//*.cpp"] 
      let os = ["_build" </> c -<.> "o" | c <- cs]
      let appFile = app </> exec -<.> "cpp"
      let osApp = "_build" </> exec -<.> "o"  -- need app.o
      need (osApp:os)
      cmd compiler cflags "-o" [out] (osApp:os)

    "_build" </> exec -<.> "o" %> \out -> do
    -- "_build/app.o" %> \out -> do
      let c = app </> dropDirectory1 out -<.> "cpp"
      putNormal ("Generating .o from main file -> " ++ c)
      let m = out -<.> "m"
      () <- cmd compiler cflags "-c" [c] "-o" [out] "-MMD -MF" [m]
      needMakefileDependencies m

    -- "_build" </> sources </> "//*.o" %> \out -> do
    --   putNormal ("Generating .O's " ++ out)
    --   let c = sources </> dropDirectory1 out -<.> "cpp"
    --   let m = out -<.> "m"
    --   () <- cmd compiler cflags "-c" [c] "-o" [out] "-MMD -MF" [m]
    --   needMakefileDependencies m
