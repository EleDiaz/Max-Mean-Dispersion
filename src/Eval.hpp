/*
 * =====================================================================================
 *
 *       Filename:  Eval.hpp
 *
 *    Description:  A especific class to eval a solution
 *
 *        Version:  1.0
 *        Created:  18/04/16 20:09:41
 *       Revision:  none
 *       Compiler:  clang
 *
 *         Author:  Eleazar Díaz Delgado(eleazardzdo at gmail dot com),
 *     University:  ULL
 *
 * =====================================================================================
 */

#pragma once

#include "Solution.hpp"


using namespace std;

/*
 * Abstract class that let defines functions `Solution -> Solution`
 * this function are used several algorithms
 */
class Eval {
public:
	virtual Solution eval(Solution solution) = 0;
};
