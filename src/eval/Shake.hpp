/*
 * =====================================================================================
 *
 *       Filename:  Shake.hpp
 *
 *    Description:  Implementation of shake eval to SVNS
 *
 *        Version:  1.0
 *        Created:  24/04/16 12:40:38
 *       Revision:  none
 *       Compiler:  clang
 *
 *         Author:  Eleazar Díaz Delgado(eleazardzdo at gmail dot com),
 *     University:  ULL
 *
 * =====================================================================================
 */

#pragma once

#include <cstdlib>
#include "../Eval.hpp"


class Shake : public Eval {
private:
	int kEval;

public:
	Shake(): kEval(0) {}

	int kMax(void) {
		return 2; // TODO:
	}

	Solution eval(Solution solution) {
		int ix = 1 + rand() % solution.getProblem()->size();

		switch (kEval) {
			case 0:
				solution.flipNode(ix);
				break;
			case 1:
				solution.addNode(ix);
				break;
			case 2:
				solution.removeNode(ix);
				break;
		}
		return solution;
	}

	Shake & in(int k) {
		kEval = k;
		return *this;
	}
};
