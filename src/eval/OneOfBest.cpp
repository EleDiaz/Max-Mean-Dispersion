#include "OneOfBest.hpp"


OneOfBest::OneOfBest(int size) :
	size(size)
{};


Solution OneOfBest::eval(Solution solution) {
	vector<Solution> rcl = vector<Solution>(size, solution);

	auto min = [&](void) {
		int ix = 0;
		for (int i = 0; i < rcl.size(); i++) {
			ix = rcl[i].getCost() > rcl[ix].getCost() ? ix : i;
		}
		return ix;
	};

	solution.mapOverFreeNodes([&](int node) {
		Solution aux = solution;
		aux.addNode(node);
		int minIx = min();
		if (rcl[minIx].getCost() < aux.getCost()) {// < <= ???
			rcl[minIx] = aux;
		}
	});

	return rcl[rand() % size];
};
