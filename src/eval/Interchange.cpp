
#include "Interchange.hpp"


Solution Interchange::eval(Solution solution) {
	Solution best = solution;

	solution.mapOverSelectNodes([&] (int rm) {
		solution.mapOverFreeNodes([&] (int add) {
			Solution aux = solution;
			aux.removeNode(rm);
			aux.addNode(add);
			if (best.getCost() < aux.getCost()) {// < <= ???
				best = aux;
			}
		});
	});
	return best;
}

