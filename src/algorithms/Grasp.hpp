/*
* =====================================================================================
*
*       Filename:  DestructorGreedy.hpp
*
*    Description:  A destructor greedy algorithm declared in task.
*
*        Version:  1.0
*        Created:  16/04/16 18:18:24
*       Revision:  none
*       Compiler:  clang
*
*         Author:  Eleazar Díaz Delgado(eleazardzdo at gmail dot com),
*     University:  ULL
*
* =====================================================================================
*/

#pragma once

#include "Algorithm.hpp"
#include "../eval/LocalSearch.hpp"
#include "../eval/OneOfBest.hpp"
#include "../eval/Interchange.hpp"


class Grasp : public Algorithm {
private:
	int sizeRCL;
public:
	Grasp(int sizeRCL);

	Solution solve(Problem * problem);

	~Grasp();
};

