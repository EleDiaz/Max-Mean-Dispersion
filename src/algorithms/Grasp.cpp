#include "Grasp.hpp"



Grasp::Grasp(int sizeRCL):
	Algorithm("Grasp RCL: " + to_string(sizeRCL)),
	sizeRCL(sizeRCL)
{
}

Solution Grasp::solve(Problem * problem) {
	auto interchange = Interchange();
	int nodeI = 1;
	int nodeJ = 1;
	problem->mapOverEdges([&](int nodeA, int nodeB, int cost) {
		if (problem->getWeight(nodeI, nodeJ) < problem->getWeight(nodeA, nodeB)) {
			nodeI = nodeA;
			nodeJ = nodeB;
		}
	});

	Solution bestAffinity = Solution::pairNode(nodeI, nodeJ, problem);
	bool stop = false;
	do {
		Solution oneOfBest = OneOfBest(sizeRCL).eval(bestAffinity);
		Solution localSearch = LocalSearch(&interchange).eval(oneOfBest); // Se intercambia nodos

		if (bestAffinity.isEqual(localSearch)) { // si no cambia vamonos, si no actualiza
			stop = true;
		}
		else {
			bestAffinity = localSearch;
		}
	} while (!stop);

	return bestAffinity;
}

Grasp::~Grasp()
{
}
