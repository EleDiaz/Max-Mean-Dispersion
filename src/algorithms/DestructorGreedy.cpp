
#include "DestructorGreedy.hpp"


DestructorGreedy::DestructorGreedy():
	Algorithm("DestructorGreedy")
{
}

Solution DestructorGreedy::solve(Problem * problem) {
	/*
		Partimos de la solucion de tener todos los nodos escogidos
		y eliminanos de forma iterativa si en la iteracion se mejora
		si dejamos de mejorar devolvemos la solucion
	*/
	Solution solution = Solution::full(problem);
	bool stop = false;
	do {
		Solution test = solution;
		Solution removeK = MaximumRemoving().eval(solution);
		if (removeK.getCost() >= solution.getCost()) {
			solution = removeK;
		}
		if (test.isEqual(removeK)) {
			stop = true;
		}
		test = solution;
	} while (!stop);

	return solution;
}


DestructorGreedy::~DestructorGreedy()
{
}
