/*
 * =====================================================================================
 *
 *       Filename:  BuidlerGreedy.hpp
 *
 *    Description:  A builder greedy algorithm declared in task.
 *
 *        Version:  1.0
 *        Created:  16/04/16 18:18:24
 *       Revision:  none
 *       Compiler:  clang
 *
 *         Author:  Eleazar Díaz Delgado(eleazardzdo at gmail dot com),
 *     University:  ULL
 *
 * =====================================================================================
 */

#ifndef BUILDER_GREEDY
#define BUILDER_GREEDY

#include "../Problem.hpp"
#include "../Solution.hpp"
#include "Algorithm.hpp"
#include "../eval/Maximum.hpp"


using namespace std;

class BuilderGreedy : public Algorithm {
private:
public:
	BuilderGreedy():
		Algorithm("BuilderGreedy") {}

	Solution solve(Problem * problem) {
		/*
		 * Selecionar la arista (i,j) con mayor afinidad
		 * S = {i,j}
		 * repeat
		 *	 S* = S
		 *	 Obtener el vertice k que maximiza md(S U {k})
		 *	 if md(S U {k}) >= md(S)
		 *	   S = S U {k}
		 * until (S* = S)
		 */
		int nodeI = 1;
		int nodeJ = 1;
		problem->mapOverEdges([&] (int nodeA, int nodeB, int cost) {
			if (problem->getWeight(nodeI, nodeJ) < problem->getWeight(nodeA, nodeB)) {
				nodeI = nodeA;
				nodeJ = nodeB;
			}
		});

		Solution bestAffinity = Solution::pairNode(nodeI, nodeJ, problem);
		bool stop = false;
		do {
			Solution test = bestAffinity;
			Solution addK = Maximum().eval(test);
			if (addK.getCost() >= test.getCost()) {
				test = addK;
			}

			if (bestAffinity.isEqual(test)) {
				stop = true;
			}
			bestAffinity = test;

		} while (!stop);
		return bestAffinity;
	}
};

#endif /* end of include guard: BUILDER_GREEDY */
