/*
 * =====================================================================================
 *
 *       Filename:  Problem.hpp
 *
 *    Description:  Specify the kind of input can accept algorithms implemented
 *
 *        Version:  1.0
 *        Created:  16/04/16 18:18:24
 *       Revision:  none
 *       Compiler:  clang
 *
 *         Author:  Eleazar Díaz Delgado(eleazardzdo at gmail dot com),
 *     University:  ULL
 *
 * =====================================================================================
 */
#pragma once

#include <functional>
#include <cstdlib>
#include <ctime>
#include <iostream>
#include <vector>
#include <tuple>

using namespace std;

typedef double Weight;

/*
 * Tener en cuenta que no es un grafo dirigido no es necesario hacer una matrix
 * solo la triangular superior
 * si se tiene un valor de inferior llevarlo a la superior
 */
class Problem {
private:
	vector<vector<Weight> > graph;

	/*
	* Permite acceder a la direccion del vector de forma directa, tal como si fuese una matriz
	* se usa de forma interna
	*/
	Weight& get(int nodeI, int nodeJ);

	/**
	* Permite acceder pero no modifica el valor
	*/
	Weight get(int nodeI, int nodeJ) const;

public:
	Problem(void);

	/*
	 * Build a problem with random numbers into it
	 */
	Problem(int size);

	/*
	 * Reads from input stream a problem
	 */
	Problem(istream & is);

	/*
	 * Build a a problem with size of problem and vector with tuple of values
	 * i,j and weight.
	 */
	Problem(int size, vector<tuple<int, int, Weight> > graphI);

	/*
	 * Get conections from determinate node
	 */
	vector<int> getConnections(int node);


	/**
	 * Map over all edges of graph
	 */
	void mapOverEdges(function<void (int, int, Weight)> func);

	/*
	 * Extracts weight of problem
	 */
	Weight getWeight(int nodeI, int nodeJ) const;

	/*
	 * Get size of Problem
	 */
	unsigned int size(void);
};
