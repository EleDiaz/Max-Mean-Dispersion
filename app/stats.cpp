/*
 * =====================================================================================
 *
 *       Filename:  stats.cpp
 *
 *    Description:  Show stadistics from diferents algorithms
 *
 *        Version:  1.0
 *        Created:  24/04/16 20:32:05
 *       Revision:  none
 *       Compiler:  gcc
 *
 *
 * =====================================================================================
 */

#include <iostream>
#include <stdlib.h>     /* srand, rand */
#include <time.h>       /* time */
#include <chrono>
#include <climits>
#include <fstream>
#include <iomanip>

#include "../src/Problem.hpp"
#include "../src/Solution.hpp"
#include "../src/algorithms/BuilderGreedy.hpp"
#include "../src/algorithms/DestructorGreedy.hpp"
#include "../src/algorithms/Grasp.hpp"
#include "../src/algorithms/MultiRun.hpp"
#include "../src/algorithms/VNS.hpp"



using namespace std;

void tests(int ntest, int size) {
	Algorithm * bg = new BuilderGreedy();
	Algorithm * dg = new DestructorGreedy();
	Algorithm * g1 = new Grasp(3);
	Algorithm * g2 = new Grasp(4);
	Algorithm * g3 = new Grasp(5);
	Algorithm * mr1 = new MultiRun(3);
	Algorithm * mr2 = new MultiRun(4);
	Algorithm * mr3 = new MultiRun(5);
	Algorithm * vns1 = new VNS(20, 1.3, diffSolution);
	Algorithm * vns2 = new VNS(50, 1.3, diffSolution);

	for (size_t i = 100; i < size; i++) {
		for (size_t j = 0; j < ntest; j++) {
			cout << "- Prueba "<< j << endl;
			cout << "|" << setw(5) << "ID |"<< setw(30)<<"Name |" << setw(10) << "Coste |" << setw(11) <<" CPU" << "|"<< endl;
			cout << "|-|-|-|-|" << endl;
			Problem problem(i);
			for (Algorithm * algo : {bg, dg, g1, g2, g3, mr1, mr2, mr3, vns1, vns2}) {
				auto t_start = std::chrono::high_resolution_clock::now();
				Solution solution = algo->solve(&problem);
				auto t_end = std::chrono::high_resolution_clock::now();

				double value = std::chrono::duration<double, std::milli>(t_end-t_start).count();

				cout << "|" << setw(5) << i
					<< "|" << setw(30) << algo->getName()
					<< "|" << setw(10) << solution.getCost()
					<< "|" <<setw(10) << value << "|"<<endl;
			}
		}
	}
}



int main(int argc, char const* argv[]) {
	string name = argv[0];
	if (argc == 3) {
		srand(time(NULL));

		tests(atoi(argv[1]), atoi(argv[2]));
	}
	else {
		cout << "USAGE: " << name << " numtest maxsize" << endl;
	}

	return 0;
}
